<?php
namespace App;

use DivisionByZeroError;
use Exception;
use InvalidArgumentException;

class ArithmaticOperation {

    public function add($num1, $num2, $imag1=0, $imag2=0) {

        $this->checkParams($num1, $num2, $imag1, $imag2);
        return $this->prepareReturnValue(["real"=>$num1 + $num2, "imag"=>$imag1 + $imag2]);        
    }

    public function subtract($num1, $num2, $imag1=0, $imag2=0) 
    {
        $this->checkParams($num1, $num2, $imag1, $imag2);
        return $this->prepareReturnValue(["real"=>$num1 - $num2, "imag"=>$imag1 - $imag2]);    
    }

    public function multiply($num1, $num2, $imag1=0, $imag2=0) 
    {
        $this->checkParams($num1, $num2, $imag1, $imag2);

        // Multiplication  :   (a+bi)(c+di) = (ac−bd) + (ad+bc)i

        $real = ($num1 * $num2) - ($imag1 * $imag2);
        $imag = ($num1 * $imag2) + ($num2 * $imag1);

        return $this->prepareReturnValue(["real"=>$real, "imag"=>$imag]); 
    }

    public function divide($num1, $num2, $imag1=0, $imag2=0) 
    {
        $this->checkParams($num1, $num2, $imag1, $imag2);

        /**
         * w = u + vi  ,  z = x + yi 
         * w/z = ((ux + vy) + (vx - uy)i) / (x**2 + y**2)
         * w/z = ( (ux + vy) / (x**2 + y**2) ) + ( (vx - uy)i / (x**2 + y**2) )
         */

        $square_sum = $num2**2 + $imag2**2;

        if($square_sum==0)
            throw new DivisionByZeroErrorException("Can not divided by zero");

        $real = (($num1 * $num2) + ($imag1 * $imag2)) / $square_sum;
        $imag = (($num2 * $imag1) - ($num1 * $imag2)) / $square_sum;

        return $this->prepareReturnValue(["real"=>$real, "imag"=>$imag]); 

    }

    private function checkParams($num1, $num2, $imag1, $imag2) {
        $this->checkNumaric($num1);
        $this->checkNumaric($num2);
        $this->checkNumaric($imag1);
        $this->checkNumaric($imag2);
    }

    private function checkNumaric($num) 
    {

        if(!is_numeric($num)){
            throw new InvalidArgumentException("'".$num."' is not a valid number");
        }
        
        return true;
    }

    private function prepareReturnValue($complex)
    {
        if($complex["imag"]==0){
            return $complex['real'];
        }

        return $complex;
    }
}
