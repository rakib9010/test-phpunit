<?php

use PHPUnit\Framework\TestCase;


class AddTest extends TestCase
{
    /** 
    * @var ArithmaticOperation
    */
    private $ar_instance;


    /**
     * This method is called before each test.
     */
    protected function setUp(): void
    {
        $this->ar_instance = new \App\ArithmaticOperation();
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown(): void
    {
    }


    /**
     * Test Add operation
     */


    public function test_add_will_through_ArgumentCountError() 
    {
        $this->expectException(ArgumentCountError::class);
        $this->ar_instance->add(100);
    }

    public function test_add_with_invalid_first_argument_will_throw_InvalidArgumentException() 
    {
        $this->expectException(InvalidArgumentException::class);        
        $this->ar_instance->add(null, 200);
    }


    public function test_add_with_invalid_second_argument_will_throw_InvalidArgumentException() 
    {
        $this->expectException(InvalidArgumentException::class);        
        $this->ar_instance->add(100, null);
    }


    public function test_add_with_two_numbers() 
    {
        /**
         * Test with integer numbers
         */
        $this->assertEquals(30, $this->ar_instance->add(20, 10));
        $this->assertEquals(110, $this->ar_instance->add(50, 60));


        /**
         * Test float numbers
         */
        $this->assertEquals(0.4, $this->ar_instance->add(0.1, 0.3));
    }


    /**
     * Test complex numbers
     */
    public function test_add_with_two_complex_numbers() 
    {
        $this->assertIsNumeric($this->ar_instance->add(0, 0, 0, 0));
        $this->assertIsNumeric($this->ar_instance->add(5, 1, 0, 0));
        $this->assertIsArray($this->ar_instance->add(2, 5, -3, 0));

        $this->assertEquals(['real'=>11, 'imag'=>15], $this->ar_instance->add(5, 6, 7, 8));
        $this->assertEquals(['real'=>30, 'imag'=>70], $this->ar_instance->add(10, 20, 30, 40));
        $this->assertEquals(['real'=>0, 'imag'=>32], $this->ar_instance->add(0, 0, 11, 21));
    }

    /**
     * @dataProvider additionProvider
     */
    public function test_add_with_provider(...$params) 
    {
        $expected = array_pop($params);
        $this->assertSame($expected, $this->ar_instance->add(...$params));
    }


    public function additionProvider(): array
    {
        return [
            [0, 0, 0],
            [5, 3, 8],
            [1, 0, 1],
            [2, 1, 3],
            [0.5, 0.6, 1.1],
            [2, -4, 6, -8, ['real'=>-2, 'imag'=>-2]],
            [5, 10, 25, 50, ['real'=>15, 'imag'=>75]],
            [0, 0, 1, ['real'=>0, 'imag'=>1]]
        ];
    }





}