<?php

use PHPUnit\Framework\TestCase;


class DivideTest extends TestCase
{
    /** 
    * @var ArithmaticOperation
    */
    private $ar_instance;


    /**
     * This method is called before each test.
     */
    protected function setUp(): void
    {
        $this->ar_instance = new \App\ArithmaticOperation();
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown(): void
    {
    }


    /**
     * Test Divition operation
     */


    public function test_divide_will_through_ArgumentCountError() 
    {
        $this->expectException(ArgumentCountError::class);
        $this->ar_instance->divide(100);
    }

    public function test_divide_with_invalid_first_argument_will_throw_InvalidArgumentException() 
    {
        $this->expectException(InvalidArgumentException::class);        
        $this->ar_instance->divide(null, 200);
    }


    public function test_divide_with_invalid_second_argument_will_throw_InvalidArgumentException() 
    {
        $this->expectException(InvalidArgumentException::class);        
        $this->ar_instance->divide(100, null);
    }

    public function test_divide_with_two_numbers() 
    {
        /**
         * Test with integer numbers
         */
        $this->assertEquals(4, $this->ar_instance->divide(20, 5));
        $this->assertEquals(5, $this->ar_instance->divide(15, 3));


        /**
         * Test float numbers
         */
        $this->assertEquals(0.5, $this->ar_instance->divide(0.1, 0.2));
    }


    public function test_division_with_divided_by_0() 
    {
        $this->expectException(\App\DivisionByZeroErrorException::class);
        $this->ar_instance->divide(100, 0);
    }



    /**
     * Test complex numbers
     */
    public function test_divide_with_two_complex_numbers() 
    {

        /**
         * w = 5 + 3i  ,  z = 6 + 2i 
         * w/z = ((ux + vy) + (vx - uy)i) / (x**2 + y**2)
         * w/z = ( (ux + vy) / (x**2 + y**2) ) + ( (vx - uy)i / (x**2 + y**2) )
         */

        // $square_sum = 36 + 4 = 40;
        // $real = ((5 * 6) + (3 * 2)) / 40  = (30 + 6)/40 = 0.9;
        // $imag = ((6 * 3) - (5 * 2)) / 40  = (18 - 10)/40 = 0.2;

        $this->assertEquals(['real'=>0.9, 'imag'=>0.2], $this->ar_instance->divide(5, 6, 3 , 2));

    }



    /**
     * @dataProvider divideionitionProvider
     */
    public function test_divide_with_provider(...$params) 
    {
        $expected = array_pop($params);
        $this->assertSame($expected, $this->ar_instance->divide(...$params));
    }


    public function divideionitionProvider(): array
    {
        return [
            [5, 2, 2.5],
            [30, 6, 5],
            [2, 1, 2],
            [0.9, 0.3, 3.0]
        ];
    }

}