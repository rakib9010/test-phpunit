<?php

use PHPUnit\Framework\TestCase;


class SubtractTest extends TestCase
{
    /** 
    * @var ArithmaticOperation
    */
    private $ar_instance;


    /**
     * This method is called before each test.
     */
    protected function setUp(): void
    {
        $this->ar_instance = new \App\ArithmaticOperation();
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown(): void
    {
    }


    /**
     * Test Subtraction operation
     */


    public function test_subtract_will_throw_ArgumentCountError() 
    {
        $this->expectException(ArgumentCountError::class);
        $this->ar_instance->subtract(100);
    }

    public function test_subtract_with_invalid_first_argument_will_throw_InvalidArgumentException() 
    {
        $this->expectException(InvalidArgumentException::class);        
        $this->ar_instance->subtract(null, 200);
    }


    public function test_subtract_with_invalid_second_argument_will_throw_InvalidArgumentException() 
    {
        $this->expectException(InvalidArgumentException::class);        
        $this->ar_instance->subtract(100, null);
    }

    public function test_subtract_with_two_numbers() 
    {
        /**
         * Test with integer numbers
         */
        $this->assertEquals(15, $this->ar_instance->subtract(20, 5));
        $this->assertEquals(-10, $this->ar_instance->subtract(50, 60));


        /**
         * Test float numbers
         */
        $this->assertEquals(-0.2, $this->ar_instance->subtract(0.1, 0.3));
    }



    /**
     * Test complex numbers
     */
    public function test_subtract_with_two_complex_numbers() 
    {
        $this->assertIsNumeric($this->ar_instance->subtract(0, 0, 0, 0));
        $this->assertIsNumeric($this->ar_instance->subtract(5, 1, 0, 0));
        $this->assertIsArray($this->ar_instance->subtract(2, 5, -3, 0));

        $this->assertEquals(['real'=>-3, 'imag'=>-3], $this->ar_instance->subtract(2, 5, -3, 0));
        $this->assertEquals(['real'=>-1, 'imag'=>-2], $this->ar_instance->subtract(5, 6, 7, 9));
        $this->assertEquals(['real'=>12, 'imag'=>10], $this->ar_instance->subtract(22, 10, 40, 30));
        $this->assertEquals(['real'=>0, 'imag'=>-10], $this->ar_instance->subtract(0, 0, 11, 21));
    }


    /**
     * @dataProvider subtractionitionProvider
     */
    public function test_subtract_with_provider(...$params) 
    {
        $expected = array_pop($params);
        $this->assertSame($expected, $this->ar_instance->subtract(...$params));
    }


    public function subtractionitionProvider(): array
    {
        return [
            [0, 0, 0],
            [5, 3, 2],
            [1, 0, 1],
            [2, 1, 1],
            [0.5, 0.6, -0.1],
            [2, -4, 6, -8, ['real'=>6, 'imag'=>14]],
            [5, 10, 25, 10, ['real'=>-5, 'imag'=>15]],
            [0, 0, 1, ['real'=>0, 'imag'=>1]]
        ];
    }

}