<?php

use PHPUnit\Framework\TestCase;


class PrivateMethodTest extends TestCase
{
    /** 
    * @var ArithmaticOperation
    */
    private $ar_instance;


    /**
     * This method is called before each test.
     */
    protected function setUp(): void
    {
        $this->ar_instance = new \App\ArithmaticOperation();
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown(): void
    {
    }


    /**
     * Test Private Method
     */

    public function test_checkNumaric_will_throw_ArgumentCountError() 
    {
        $this->expectException(ArgumentCountError::class);
        $this->invokeMethod($this->ar_instance, 'checkNumaric');
    }

    public function test_checkNumaric_with_invalid_first_argument_will_throw_InvalidArgumentException() 
    {
        $this->expectException(InvalidArgumentException::class);        
        $this->invokeMethod($this->ar_instance, 'checkNumaric', ['invalid_number']);
    }



    public function test_checkNumaric_with_valid_number_will_return_true() 
    {     
        $this->assertTrue($this->invokeMethod($this->ar_instance, 'checkNumaric', [100]));
        $this->assertTrue($this->invokeMethod($this->ar_instance, 'checkNumaric', ['100']));
        $this->assertTrue($this->invokeMethod($this->ar_instance, 'checkNumaric', [0]));
        $this->assertTrue($this->invokeMethod($this->ar_instance, 'checkNumaric', [-50]));
    }


    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }



}