<?php

use PHPUnit\Framework\TestCase;


class MultiplyTest extends TestCase
{
    /** 
    * @var ArithmaticOperation
    */
    private $ar_instance;


    /**
     * This method is called before each test.
     */
    protected function setUp(): void
    {
        $this->ar_instance = new \App\ArithmaticOperation();
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown(): void
    {
    }


    /**
     * Test multiply operation
     */


    public function test_multiply_will_throw_ArgumentCountError() 
    {
        $this->expectException(ArgumentCountError::class);
        $this->ar_instance->multiply(100);
    }

    public function test_multiply_with_invalid_first_argument_will_throw_InvalidArgumentException() 
    {
        $this->expectException(InvalidArgumentException::class);        
        $this->ar_instance->multiply(null, 200);
    }


    public function test_multiply_with_invalid_second_argument_will_throw_InvalidArgumentException() 
    {
        $this->expectException(InvalidArgumentException::class);        
        $this->ar_instance->multiply(100, null);
    }

    public function test_multiply_with_two_numbers() 
    {
        /**
         * Test with integer numbers
         */
        $this->assertEquals(100, $this->ar_instance->multiply(20, 5));
        $this->assertEquals(3000, $this->ar_instance->multiply(50, 60));


        /**
         * Test float numbers
         */
        $this->assertEquals(0.03, $this->ar_instance->multiply(0.1, 0.3));
    }


    public function test_multiply_check_sign_of_the_return_value() 
    {
        $this->assertGreaterThanOrEqual(0, $this->ar_instance->multiply(0, 0));
        $this->assertGreaterThanOrEqual(0, $this->ar_instance->multiply(-1, -1));
        $this->assertGreaterThanOrEqual(0, $this->ar_instance->multiply(2, 3));
        $this->assertLessThan(0, $this->ar_instance->multiply(100, -1));
        $this->assertLessThan(0, $this->ar_instance->multiply(-10, 5));
    }



    /**
     * Test complex numbers
     */
    public function test_subtract_with_two_complex_numbers() 
    {
        // $this->assertIsNumeric($this->ar_instance->subtract(0, 0, 0, 0));
        // $this->assertIsNumeric($this->ar_instance->subtract(5, 1, 0, 0));
        // $this->assertIsArray($this->ar_instance->subtract(2, 5, -3, 0));

        // (2 + 5i)(4 - 3i) = 23+14i
        // $real = (2 * 4) - (5 * (-3)); 23
        // $imag = (2 * (-3)) + (4 * 5);  14i
        $this->assertEquals(['real'=>23, 'imag'=>14], $this->ar_instance->multiply(2, 4, 5 ,-3));


        // (15i)(- 2i) = 30
        // $real = (0 * 0) - (15 * (-2)); 30
        // $imag = (0 * (-3)) + (0 * 5);  0
        $this->assertEquals(30, $this->ar_instance->multiply(0, 0, 15 ,-2));
    }


    /**
     * @dataProvider multiplyionitionProvider
     */
    public function test_multiply_with_provider(...$params) 
    {
        $expected = array_pop($params);
        $this->assertSame($expected, $this->ar_instance->multiply(...$params));
    }


    public function multiplyionitionProvider(): array
    {
        return [
            [0, 0, 0],
            [5, 3, 15],
            [-1, 10, -10],
            [2, -1, -2],
            [-2, -2, 4],
            [0.5, 0.6, 0.3]
        ];
    }

}